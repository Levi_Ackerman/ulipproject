package com.project.ulip;

import org.openqa.selenium.WebDriver;



public class Base {

	public static void main(String[] args) {
		
		Initializer initializer = new Initializer();
		WebDriver driver = initializer.init();
		
		//For AegonLife
		initializer.openAegonlife(driver);
		Aegonlife al = new Aegonlife();
		al.selectSchemeName(driver);
		al.selectStartDate(driver);
		al.selectEndDate(driver);
		al.clickSearch(driver);
		al.pickDate(driver);
		al.pickPrice(driver);
		
		//For Aditya Birla
		initializer.openAdityabirla(driver);
		BirlaLifePlan b = new BirlaLifePlan();
		b.pickDate(driver);
		b.pickPrice(driver);
		
		//For Tata AIA
		initializer.openTataaia(driver);
		TataLifePlan tt = new TataLifePlan();
		tt.selectFund(driver);
		tt.selectDay(driver);
		tt.selectMonth(driver);
		tt.selectYear(driver);
		tt.clickSubmit(driver);
		tt.pickDate(driver);
		tt.picPrice(driver);

	}

}
